---
title: "Simpson's Paradox"
output: html_notebook
---


```{r}
library(corrplot)
library(tidyverse)
```


```{r}
smoking <- read.csv("data/smoking.csv")
smoking
dplyr::glimpse(smoking)
```


```{r}
infos <- smoking %>% group_by(Smoker, Status) %>% count() %>% mutate(count=n) %>% select(Smoker, Status, count)
infos <- infos %>% ungroup(Status) %>% mutate(alive=ifelse(Status=="Alive", count, NA)) %>% mutate(dead=ifelse(Status=="Dead", count, NA))
infos
```


```{r}
ggplot(infos, aes(x=Smoker, y=count, fill=Status)) + geom_bar(stat='identity', position='dodge')
```

In the above plot we can see that the number of deaths in the non smoker group is higher than the one in the smoker group, which is surprising as we expect to see the aposite. However this might just be due to the fact that we have more data for the non smoker group compared to the smoker group.

```{r}
gp_infos <- infos %>% ungroup(Status) %>% group_by(Smoker) %>% zoo::na.locf() %>% filter(row_number()==n()) %>% select(Smoker, alive, dead)
```

```{r}
gp_infos$ratio = gp_infos$dead/(gp_infos$alive+gp_infos$dead)
gp_infos
```

As expected from the initial analysis, the mortality rate in the non smoker group (31%) is higher than the one in the smoker group (23%).

```{r}
infos$Smoker[infos$Smoker=="No"] <- 0
infos$Smoker[infos$Smoker=="Yes"] <- 1
```


```{r}
infos$Smoker_F = as.factor(infos$Smoker)
with(infos, plot(count~Smoker_F))
infos
```

We can see from the above confidence intervals that the smokers confidence interval is larger than the non smokers confidence interval which is due to having more data on non smoker compared to smokers


## 2.Age categories

```{r}
smoking$Age_cat[smoking$Age>=18 && smoking$Age<34] <- "18-33"
smoking$Age_cat[smoking$Age>=34 && smoking$Age<55] <- "34-54"
smoking$Age_cat[smoking$Age>=55 && smoking$Age<65] <- "55-64"
smoking$Age_cat[smoking$Age>=65] <- "over 65"
smoking
```

```{r}
ggplot(smoking, aes(x=Age_cat, y=Status, color=Status)) + geom_point()
```
