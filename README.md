# Scientific Methodology


## Related resources

[Course website](https://github.com/alegrand/SMPE/tree/master/sessions/2022_10_Grenoble#readme) 
    
[Pad](http://pads.univ-grenoble-alpes.fr/p/MOSIG-SMPE-2022-2023) 


## Homeworks
- [homework 1](notebooks/homework-1.Rmd)
- [homework 2](notebooks/homework-1.Rmd)
- [challenger](notebooks/challenger.ipynb)
- [Names homework notebook](notebooks/03_Names-Methodo2022-exercise.Rmd) [, html version](docs/03_Names-Methodo2022-exercise.html)
- [Quick sort notebook](https://github.com/maryam21/M2R-ParallelQuicksort/blob/master/analysis/quickSort.Rmd) , [html](https://github.com/maryam21/M2R-ParallelQuicksort/blob/master/analysis/quickSort.html), [updated code](https://github.com/maryam21/M2R-ParallelQuicksort/blob/master/src/parallelQuicksort.c), [experiment generation script](https://github.com/maryam21/M2R-ParallelQuicksort/blob/master/scripts/gen_experiments.R), [script for running experiment](https://github.com/maryam21/M2R-ParallelQuicksort/blob/master/scripts/run_experiment.sh)
- [Trees](notebooks/trees.Rmd), [html](docs/trees.html)
- [Batman](notebooks/batman.Rmd), [html](docs/batman.html)
- [shinyapp](notebooks/shiny_app.Rmd), [html](docs/shiny_app.html)
- [Ethics](docs/Ethics_presentation.pdf) , [report](docs/Ethics.pdf)
- [Module 3 report](docs/module3_report.pdf)
- [Simpson's Paradox](notebooks/smoke.Rmd)



